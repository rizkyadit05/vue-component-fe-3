import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import PlayGround from './views/PlayGround.vue';
import PlayGroundSideA from './components/PlayGroundSideA.vue';
import PlayGroundSideB from './components/PlayGroundSideB.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/playground',
      name: 'playground',
      component: PlayGround,
      children: [
        {
          path: 'side-a',
          name: 'pg-side-a',
          component: PlayGroundSideA
        },
        {
          path: 'side-b',
          name: 'pg-side-b',
          component: PlayGroundSideB
        }
      ]
    }
  ]
});
