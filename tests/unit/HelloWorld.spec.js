import { shallowMount } from '@vue/test-utils';
import HelloWorld from '@/components/HelloWorld.vue';

describe('HelloWorld.vue', () => {
  it('renders div', () => {
    const wrapper = shallowMount(
      HelloWorld, {
        mocks: {
          $store: {
            state: {}
          }
        }
      }
    );
    expect(wrapper.contains('div')).toBe(true);
  });

  it('renders props.msg when passed', () => {
    const msg = 'new message';
    const wrapper = shallowMount(HelloWorld, {
      propsData: { msg },
      mocks: {
        $store: {
          state: {}
        }
      }
    });
    expect(wrapper.text()).toMatch(msg);
  });
});
